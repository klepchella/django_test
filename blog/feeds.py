from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords

from .models import Post


class LatestPostsFeed(Feed):
    title = 'My blog'
    link = '/blog/'
    description = 'New post'

    def items(self):
        return Post.published.all()

    def item_title(self, item):
        return Post.published.all().order_by('-publish')[:5]

    def item_description(self, item):
        return truncatewords(item.body, 30)